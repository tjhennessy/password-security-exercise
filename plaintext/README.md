# Password Evolution Plaintext Password Storage Program

This program is designed to show the vulnerability in a program that
stores passwords in plaintext.

## Install

```bash
$ python3 -m venv venv
$ source venv/bin/activate
(venv) $ python3 -m pip install -r requirements.txt
```

## Run 

```bash
(venv) $ nohup python3 password-evolution.py &
```

## Kill

```bash
(venv) $ pkill -f password-evolution.py
(venv) $ deactivate
```

Alternatively, you could kill the process using the below instructions:
```bash
(venv) $ lsof -i :5000
```

That should return the PID of the process running on port 5000 (our program)
```bash
(venv) $ kill -9 $PID
(venv) $ deactivate
```

## Usage

Sign up with username and password
```bash
$ curl -k -X POST -F 'username=alice' -F 'password=P@$sw0rD!' 'https://localhost:5000/signup/v1'
```
Should return 'signup success'

Login 
```bash
$ curl -k -X POST -F 'username=alice' -F 'password=P@$sw0rD!' 'https://localhost:5000/login/v1'
```
Should return 'login success'
