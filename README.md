# Password Security Practical Example

This directory contains two subdirectories each containing
code which demonstrates how a user can signup and login to a
web service. One is in plaintext and the other is hashed.
